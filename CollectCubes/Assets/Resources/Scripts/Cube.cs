﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    [SerializeField] private Material _collectedMaterial;
    public void OnCollected()
    {
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Collider>().isTrigger = true;
        GetComponent<MeshRenderer>().material = new Material(_collectedMaterial);
    }

}
