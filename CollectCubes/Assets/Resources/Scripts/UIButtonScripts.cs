﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIButtonScripts : MonoBehaviour
{
    [SerializeField] private InputField _input;
    [SerializeField] private Text _errorText;
    [SerializeField] private Material _noncollectedMaterial;
    private GameObject _gameManagerObj;
    private void Start()
    {
        _gameManagerObj = GameObject.Find("GameManager(Clone)");
        if (_gameManagerObj == null)
            _gameManagerObj = Instantiate(Resources.Load<GameObject>("Prefabs/GameManager") as GameObject);
        DontDestroyOnLoad(_gameManagerObj);
    }

    public void LoadLevel1()
    {
        _gameManagerObj.GetComponent<GameManager>().SetNewLevel(199);
        SceneManager.LoadScene("Level");
        _gameManagerObj.GetComponent<GameManager>().CreateNewLevel();
    }
    public void LoadLevel5000()
    {
        _gameManagerObj.GetComponent<GameManager>().SetNewLevel(4999);
        SceneManager.LoadScene("LoadingScreen");
    }

    public void LoadSpecial()
    {
        if (_input.text.Equals(""))
        {
            _errorText.text = "Please enter some number for the object count.";
        }
        else
        {
            var count = Int32.Parse(_input.text);
            _gameManagerObj.GetComponent<GameManager>().SetNewLevel(count);
            SceneManager.LoadScene("LoadingScreen");
        }
    }

    public void OpenMainMenu()
    {
        var _poolManager = GameObject.Find("PoolManager").GetComponent<PoolManager>();
        foreach (var cube in _poolManager.CubeList.FindAll(x => x.activeInHierarchy))
        {
            cube.GetComponent<Rigidbody>().isKinematic = false;
            cube.GetComponent<Collider>().isTrigger = false;
            cube.GetComponent<MeshRenderer>().material = new Material(_noncollectedMaterial);
            cube.SetActive(false);
        }
        SceneManager.LoadScene("MainMenu");
    }
}