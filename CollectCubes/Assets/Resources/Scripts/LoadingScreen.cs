﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    void Start()
    {
        var _gameManagerObj = GameObject.Find("GameManager(Clone)");
        var _poolManager = GameObject.Find("PoolManager").GetComponent<PoolManager>();
        if (_poolManager.CubeList.Count < _gameManagerObj.GetComponent<GameManager>().AllCubes)
        {
            _poolManager.NeedMore(_gameManagerObj.GetComponent<GameManager>().AllCubes - _poolManager.CubeList.Count, _gameManagerObj);
        }
        else
        {
            SceneManager.LoadScene("Level");
            _gameManagerObj.GetComponent<GameManager>().CreateNewLevel();
        }
    }
}
