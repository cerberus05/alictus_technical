﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    private int _collectedCubes;
    public int AllCubes;
    private float _timer;
    private PoolManager _poolManager;

    private void Start()
    {
        Instance = this;
    }

    public void SetNewLevel(int count)
    {
        AllCubes = count;
        AllCubes = 100 * ((AllCubes / 100) + 1);
        _poolManager = GameObject.Find("PoolManager").GetComponent<PoolManager>();
    }

    public void CreateNewLevel()
    {
        int height = 2 * (int)Mathf.Sqrt(AllCubes / 2);
        int width = height / 2;
        for (int i = -height / 2; i < height / 2; i++)
        {
            for (int j = -width / 2; j < width / 2; j++)
            {
                var cube = _poolManager.CubeList.First(x => x.activeInHierarchy == false);
                cube.transform.position = new Vector3(-i, 2.5f, -j);
                cube.gameObject.SetActive(true);
            }
        }
    }

    public void OnNewCubeCollected()
    {
        _collectedCubes++;
        if (_collectedCubes == AllCubes)
        {
            GameFinished();
        }
    }

    private void GameFinished()
    {
        GameObject.Find("Canvas").transform.GetChild(1).gameObject.SetActive(true);
    }
}
