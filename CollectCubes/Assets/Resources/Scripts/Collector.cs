﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        var cubeScript = collision.gameObject.GetComponent<Cube>();
        if (cubeScript != null)
        {
            cubeScript.OnCollected();
            GameManager.Instance.OnNewCubeCollected();
        }
    }
}
