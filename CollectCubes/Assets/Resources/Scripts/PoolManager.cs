﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PoolManager : MonoBehaviour
{
    private GameObject _cubePrefab;
    public List<GameObject> CubeList;
    private void Start()
    {
        _cubePrefab = Resources.Load<GameObject>("Prefabs/Cube") as GameObject;
        CubeList = new List<GameObject>();
        for (int i = 0; i < 500; i++)
        {
            var newCube = Instantiate(_cubePrefab);
            newCube.gameObject.SetActive(false);
            CubeList.Add(newCube);
            DontDestroyOnLoad(newCube);
        }
        DontDestroyOnLoad(this.gameObject);
        SceneManager.LoadScene("MainMenu");
    }

    public void NeedMore(int x, GameObject _gameManagerObj)
    {
        for (int i = 0; i < x + 500; i++)
        {
            var newCube = Instantiate(_cubePrefab);
            newCube.gameObject.SetActive(false);
            CubeList.Add(newCube);
            DontDestroyOnLoad(newCube);
        }
        SceneManager.LoadScene("Level");
        _gameManagerObj.GetComponent<GameManager>().CreateNewLevel();
    }
}
