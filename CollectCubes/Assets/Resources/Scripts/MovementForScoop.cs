﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementForScoop : MonoBehaviour
{
    private float _rotateSpeed = 5f;
    [SerializeField] LayerMask _layer;
    private float _moveSpeed = 25f;
    private Vector3 _targetPos;
    private bool _isMoving, _isRotating;

    private void Start()
    {
        _targetPos = transform.position;
        _isMoving = false;
        _isRotating = false;
    }

    void FixedUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 2000f, _layer))
        {
            _targetPos = new Vector3(hit.point.x, 2.5f, hit.point.z);
            if (Vector3.Distance(transform.position, _targetPos) > 25f)
            {
                _isMoving = true;
                _isRotating = true;
            }
        }

        if (_isMoving)
        {
            MoveObject();
        }

        if (_isRotating)
        {
            Rotator(hit);
        }
    }

    private void Rotator(RaycastHit hit)
    {
        Vector3 direction = hit.point - transform.position;
        float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.up);
        rotation = new Quaternion(rotation.x, 0, rotation.z, rotation.w);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, _rotateSpeed * Time.deltaTime);
    }

    private void MoveObject()
    {
        transform.LookAt(_targetPos);
        transform.position = Vector3.MoveTowards(transform.position, _targetPos, _moveSpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, _targetPos) < 25f)
        {
            _isRotating = false;
            _isMoving = false;
        }
    }
}
